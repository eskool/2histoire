site_name: "2nde Histoire"
site_url: https://eskool.gitlab.io/2histoire/
site_author: eSkool, Rodrigo Schwencke
site_description: >-
  This is my nice Site!

# Repository
repo_name: eskool/2histoire
repo_url: https://gitlab.com/eskool/2histoire
# edit_uri: ""
edit_uri: tree/main/docs/

# Copyright
copyright: Copyleft &#127279; 2023 - eSkool - Rodrigo Schwencke - Licence CC BY-NC-SA 4.0

# Configuration
theme:
  name: material
  custom_dir: overrides
  logo: assets/images/eskoolBlanc.svg
  favicon: assets/images/eskoolBlanc.svg
  language: fr
  features:
    # empêche nouvelle requête:
    # ! ne pas activer 'navigate.instant':
    # - navigation.instant
    - navigation.tabs
    - navigation.expand
    - navigation.top
    # Menus intégrés, ou pas:
    # - toc.integrate
    - header.autohide
    - navigation.tabs.sticky
    - navigation.sections
    # laisser 'navigation.indexes commenté, sinon les fichiers par défaut /index.md 
    # ne sont pas automatiquement affichés dans menu gauche
    # - navigation.indexes
  palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: red
        accent: red
        toggle:
            icon: material/weather-sunny
            name: Passer au mode jour

  favicon: assets/images/favicon.png
  icon:
    logo: logo
    admonition:
      note: octicons/tag-16
      abstract: octicons/checklist-16
      info: octicons/info-16
      tip: octicons/squirrel-16
      success: octicons/check-16
      question: octicons/question-16
      warning: octicons/alert-16
      failure: octicons/x-circle-16
      danger: octicons/zap-16
      bug: octicons/bug-16
      example: octicons/beaker-16
      quote: octicons/quote-16

# Extensions
markdown_extensions:
  - admonition
  - abbr
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - mkdocs_graphviz
  - mkdocs_asy
  - meta
  - pymdownx.arithmatex:
      generic: true
      smart_dollar: false
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
      options:
        custom_icons:
          - overrides/.icons
  - pymdownx.highlight:
      linenums: true
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets
  # - pymdownx.superfences
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_div_format
        # - name: graphviz
        #   class: graphviz
        #   format: !!python/name:graphviz.superfences_graphviz.format
        #   validator: !!python/name:graphviz.superfences_graphviz.validate
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - toc:
      permalink: ⚓︎
      toc_depth: 3

# Plugins
plugins:
  - search
  - kroki
  - macros
  # - markmap
  # - mkdocs-jupyter:
  #     include_source: True

      # execute: True
  # - markmap:
  #     base_path: docs
  #     encoding: utf-8
  #     file_extension: .mm.md
  #     d3_version: 6.7.0
  #     lib_version: 0.11.5
  #     view_version: 0.2.6

  # PAS le contraire : index.md -> bases.md
  # car n'affiche pas les menus
  # - redirects:
  #     redirect_maps:
  #       web/ihm_python.md: python/ihm.md

# Customization
extra:
  massilia:
    badges:
      default: deeppink
    colors:
      demo1: ff0000 0000ff b0b000 fdfd75 0000ff ff0000
    styles:
      test:
        light:
          font-family: Source Code Pro 
          background-color: purple
          color: red
          border-radius: 20px
          box-shadow : 10px 10px 5px orange
        dark:
          font-family: Source Code Pro
          background-color: cyan
          color: darkblue
          border-radius: 20px
          box-shadow : 10px 10px 5px orange
  social:
    - icon: fa/creative-commons
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-by
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-nc-eu
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-sa
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/paper-plane
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      link: mailto:contact.eskool@gmail.com
      name: Envoyer un email
    - icon: fa/gitlab
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      link: https://gitlab.com/eskool/1geographie
      name: dépôt gitlab
    - icon: fa/university
      link: https://eskool.gitlab.io/accueil
      name: The eSkool Initiative
  raw_url: https://eskool.github.io/1geographie/-/raw/main/docs/
  io_url: https://eskool.gitlab.io/1geographie

extra_css:
  - https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css
  - https://pyscript.net/latest/pyscript.css
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia.css

extra_javascript:
  # for MathJax (mathjax.js config file MUST precede 'polyfill' and 'cdn')
  # - assets/javascripts/mathjax.js
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/mathjax.js
  - https://polyfill.io/v3/polyfill.js?features=es5,es6,es7,es8&flags=gated
  - https://cdn.jsdelivr.net/npm/mathjax@3.2.2/es5/tex-mml-chtml.js

  - https://cdnjs.cloudflare.com/ajax/libs/mermaid/9.1.7/mermaid.min.js
  # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia.js
  - https://cdn.plot.ly/plotly-2.6.3.min.js

#########################################################################################
#                     MENUS DE NAVIGATION  DU SITE
#                   !!! NE RIEN ÉCRIRE CI-DESSUS !!!!
#########################################################################################

nav:
  - Accueil:
    - Sous-menu A:
      - sous-sous-menu A: index.md
      - sous-sous-menu B: soussousmenuB.md
    - Sous-menu B: soussousmenuB.md
    - Sous-menu C: soussousmenuB.md
    - Site 2nde Géographie <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/2geographie
  - Le Monde Méditerranéen: mondeMediterraneen/index.md
  - XV-XVIème Siècles: XV_XVI/index.md
  - XVII-XVIIIèmes Siècles: XVII_XVIII_DynamiquesEtRuptures/index.md
  - L'État Moderne: etatEpoqueModerne/index.md
  - Ressources: ressources/index.md

